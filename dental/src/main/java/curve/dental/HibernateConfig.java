package curve.dental;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import curve.dental.controller.AppointmentController;
import curve.dental.controller.DoctorController;
import curve.dental.controller.PatientController;
import curve.dental.model.Appointment;
import curve.dental.model.Block;
import curve.dental.model.Doctor;
import curve.dental.model.Patient;
import curve.dental.service.AppointmentService;
import curve.dental.service.AppointmentServiceImpl;
import curve.dental.service.BlockService;
import curve.dental.service.BlockServiceImpl;
import curve.dental.service.DoctorService;
import curve.dental.service.DoctorServiceImpl;
import curve.dental.service.PatientService;
import curve.dental.service.PatientServiceImpl;

@Configuration
@EnableTransactionManagement
public class HibernateConfig {

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan("curve.dental");
		sessionFactory.setHibernateProperties(hibernateProperties());
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/mydb");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager hibernateTransactionManager() {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager();
		transactionManager.setSessionFactory(sessionFactory().getObject());
		return transactionManager;
	}
	
	private Properties hibernateProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "update");
		properties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQL8Dialect");
		return properties;
	}
	
	@Bean
	public DoctorService doctorService() {
		return new DoctorServiceImpl();
	}
	
	@Bean
	public AppointmentService appointmentService() {
		return new AppointmentServiceImpl();
	}
	
	@Bean
	public BlockService blockService() {
		return new BlockServiceImpl();
	}
	
	@Bean
	public PatientService patientService() {
		return new PatientServiceImpl();
	}
	
	@Bean
	public GenericHandler<Doctor> doctorHandler() {
		GenericHandler<Doctor> doctorHandler = new GenericHandler<Doctor>();
		doctorHandler.setClass(Doctor.class);
		return doctorHandler;
	}
	
	@Bean
	public GenericHandler<Appointment> appointmentHandler() {
		GenericHandler<Appointment> appointmentHandler = new GenericHandler<Appointment>();
		appointmentHandler.setClass(Appointment.class);
		return appointmentHandler;
	}
	
	@Bean
	public GenericHandler<Block> blockHandler() {
		GenericHandler<Block> blockHandler = new GenericHandler<Block>();
		blockHandler.setClass(Block.class);
		return blockHandler;
	}
	
	@Bean
	public GenericHandler<Patient> patientHandler() {
		GenericHandler<Patient> patientHandler = new GenericHandler<Patient>();
		patientHandler.setClass(Patient.class);
		return patientHandler;
	}
	
	@Bean
	public DoctorController doctorController() {
		return new DoctorController();
	}
	
	@Bean
	public PatientController patientController() {
		return new PatientController();
	}
	
	@Bean
	public AppointmentController appointmentController() {
		return new AppointmentController();
	}
	
	@Bean Validator validator() {
		return new Validator();
	}
	
}
