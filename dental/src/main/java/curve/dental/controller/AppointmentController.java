package curve.dental.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Appointment;
import curve.dental.model.Doctor;
import curve.dental.model.Patient;
import curve.dental.service.AppointmentService;
import curve.dental.service.DoctorService;
import curve.dental.service.PatientService;

@ManagedBean
@Scope("request")
@Controller
public class AppointmentController extends SpringBeanAutowiringSupport {

	private List<Appointment> results;
	private Appointment appointment;
	private List<Doctor> doctors;
	private List<Patient> patients;

	@Autowired
	private AppointmentService appointmentService;

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PatientService patientService;

	@PostConstruct
	public void init() {
		results = appointmentService.selectAll();
		doctors = doctorService.selectAll();
		patients = patientService.selectAll();
		appointment = new Appointment();
	}

	public List<Appointment> getResults() {
		return results;
	}

	public Appointment getAppointment() {
		return appointment;
	}

	public void setAppointment(Appointment appointment) {
		this.appointment = appointment;
	}

	public void addAppointment() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(appointment.getDateStart());
		cal.add(Calendar.HOUR, 1);
		Date dateEnd = cal.getTime();
		appointment.setDateEnd(dateEnd);
		
		appointmentService.create(appointment);
		
		results = appointmentService.selectAll();
		appointment = new Appointment();
	}

	public void editAppointment(Appointment appointment) {
		appointmentService.update(appointment);
		results = appointmentService.selectAll();
	}

	public void deleteAppointment(Appointment appointment) {
		appointmentService.delete(appointment);
		results = appointmentService.selectAll();
	}

	public void startEdit(Appointment appointment) {
		appointment.setView(true);
	}

	public void stopEdit(Appointment appointment) {
		appointment.setView(false);
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	public List<Patient> getPatients() {
		return patients;
	}

	public void setPatients(List<Patient> patients) {
		this.patients = patients;
	}

}
