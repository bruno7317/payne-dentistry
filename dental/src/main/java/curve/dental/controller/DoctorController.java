package curve.dental.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Doctor;
import curve.dental.service.DoctorService;

@ManagedBean
@Scope("request")
@Controller
public class DoctorController extends SpringBeanAutowiringSupport {

	private List<Doctor> results;
	private Doctor doctor;

	@Autowired
	private DoctorService doctorService;

	@PostConstruct
	public void init() {
		results = doctorService.selectAll();
		doctor = new Doctor();
	}

	public List<Doctor> getResults() {
		return results;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public void addDoctor() throws Exception {
		try {
			doctorService.create(doctor);
			results = doctorService.selectAll();
			doctor = new Doctor();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Something went wrong");
		}
	}

	public void editDoctor(Doctor doctor) {
		doctorService.update(doctor);
		results = doctorService.selectAll();
	}

	public void deleteDoctor(Doctor doctor) {
		doctorService.delete(doctor);
		results = doctorService.selectAll();
	}

	public void startEdit(Doctor doctor) {
		doctor.setView(true);
	}
	
	public void stopEdit(Doctor doctor) {
		doctor.setView(false);
	}

}
