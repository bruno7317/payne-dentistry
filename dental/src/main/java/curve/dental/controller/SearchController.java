package curve.dental.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Doctor;
import curve.dental.service.DoctorService;

@ManagedBean
@Scope
@Controller
public class SearchController extends SpringBeanAutowiringSupport {
	
	private String criteria;
	private List<Doctor> results;
	private boolean vResults = false;
	
	@Autowired
	private DoctorService doctorService;
	
	public List<Doctor> getResults() {
		results = doctorService.selectAll();
		return results;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public String getCriteria() {
		return criteria;
	}

	public boolean isvResults() {
		return vResults;
	}

}
