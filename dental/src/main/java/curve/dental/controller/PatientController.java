package curve.dental.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Patient;
import curve.dental.service.PatientService;

@ManagedBean
@Scope("request")
@Controller
public class PatientController extends SpringBeanAutowiringSupport {

	private List<Patient> results;
	private Patient patient;

	@Autowired
	private PatientService PatientService;

	@PostConstruct
	public void init() {
		results = PatientService.selectAll();
		patient = new Patient();
	}

	public List<Patient> getResults() {
		return results;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public void addPatient() throws Exception {
		try {
			PatientService.create(patient);
			results = PatientService.selectAll();
			patient = new Patient();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Something went wrong");
		}
	}

	public void editPatient(Patient patient) {
		PatientService.update(patient);
		results = PatientService.selectAll();
	}

	public void deletePatient(Patient patient) {
		PatientService.delete(patient);
		results = PatientService.selectAll();
	}

	public void startEdit(Patient patient) {
		patient.setView(true);
	}

	public void stopEdit(Patient patient) {
		patient.setView(false);
	}

}
