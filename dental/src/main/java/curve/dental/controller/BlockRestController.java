package curve.dental.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import curve.dental.model.Block;
import curve.dental.service.BlockService;

@RestController
@RequestMapping(value = "block")
public class BlockRestController {

	private final BlockService service;

	public BlockRestController(BlockService service) {
		this.service = service;
	}
	
	@PostMapping(value = "/add", consumes = "application/json")
	@ResponseStatus(code = HttpStatus.CREATED)
	public void addAttribute(@RequestBody(required = true) Block block) {
		service.create(block);
	}

}
