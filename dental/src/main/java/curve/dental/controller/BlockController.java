package curve.dental.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Block;
import curve.dental.model.BlockDay;
import curve.dental.model.Doctor;
import curve.dental.service.BlockService;
import curve.dental.service.DoctorService;

@ManagedBean
@Scope("request")
@Controller
public class BlockController extends SpringBeanAutowiringSupport {

	private List<Block> results;
	private Block block;
	private List<Doctor> doctors;
	private List<String> selectedDays;
	private Date repeatEnd;

	@Autowired
	private BlockService blockService;

	@Autowired
	private DoctorService doctorService;

	@PostConstruct
	public void init() {
		results = blockService.selectAll();
		doctors = doctorService.selectAll();
		block = new Block();
		repeatEnd = new Date();
	}

	public List<Block> getResults() {
		return results;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	public void addBlock() {
		if (repeatEnd != null) {
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(repeatEnd);
			
		    long diffInMillies = Math.abs(cal2.getTimeInMillis() - cal1.getTimeInMillis())	;
		    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
		    diff = Math.floorDiv(diff, 7);
			block.setRepeat((int) diff);
			BlockDay blockDay = new BlockDay();
			for (int i = 0; i < selectedDays.size(); i++) {
				Integer selectedDay = Integer.parseInt(selectedDays.get(i));
				blockDay.setWeekday(selectedDay);
				block.addSelectedDay(blockDay);
				blockDay = new BlockDay();
			}
		}
		
		blockService.create(block);

		results = blockService.selectAll();
		block = new Block();
	}

	public void editBlock(Block block) {
		blockService.update(block);
		results = blockService.selectAll();
	}

	public void deleteBlock(Block block) {
		blockService.delete(block);
		results = blockService.selectAll();
	}

	public void startEdit(Block block) {
		block.setView(true);
	}

	public void stopEdit(Block block) {
		block.setView(false);
	}

	public List<Doctor> getDoctors() {
		return doctors;
	}

	public void setDoctors(List<Doctor> doctors) {
		this.doctors = doctors;
	}

	public List<String> getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(List<String> selectedDays) {
		this.selectedDays = selectedDays;
	}

	public Date getRepeatEnd() {
		return repeatEnd;
	}

	public void setRepeatEnd(Date repeatEnd) {
		this.repeatEnd = repeatEnd;
	}

}
