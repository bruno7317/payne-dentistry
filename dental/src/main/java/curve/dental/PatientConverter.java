package curve.dental;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Patient;
import curve.dental.service.PatientService;

@FacesConverter(value = "patientConverter")
@Component
public class PatientConverter extends SpringBeanAutowiringSupport implements Converter {
	
	@Autowired
	private PatientService patientService;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Patient patient = null;
		if (value != null && !value.isEmpty()) {
			int id = Integer.parseInt(value);
			patient = patientService.findById(id);
		}
		return patient;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String r = "";
		if (value != null) {
			Patient patient = (Patient) value;
			int id = patient.getId();
			r = String.valueOf(id);
		}
		return r;
	}

}
