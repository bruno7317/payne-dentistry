package curve.dental;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class GenericHandler<T extends Serializable> extends AbstractHandler<T> {

	public boolean isPeriodAvailable(int doctor, Date start, Date end) {
		return (isPeriodAvailableForAppointment(doctor, start, end) && isPeriodAvailableForBlocking(doctor, start, end));
	}

	private boolean isPeriodAvailableForBlocking(int doctor, Date start, Date end) {
		String hql = "FROM Block B WHERE B.doctor.id = :id and B.dateStart between :from and :to";
		Session session = getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter("id", doctor);
		query.setParameter("from", start);
		query.setParameter("to", end);
		List results = query.getResultList();
		return !(results != null && !results.isEmpty());
	}

	private boolean isPeriodAvailableForAppointment(int doctor, Date start, Date end) {
		String hql = "FROM Appointment A WHERE A.doctor.id = :id and A.dateStart between :from and :to";
		Session session = getCurrentSession();
		Query query = session.createQuery(hql);
		query.setParameter("id", doctor);
		query.setParameter("from", start);
		query.setParameter("to", end);
		List results = query.getResultList();
		return !(results != null && !results.isEmpty());
	}
}
