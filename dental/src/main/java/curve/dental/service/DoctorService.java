package curve.dental.service;

import curve.dental.model.Doctor;

public interface DoctorService extends GenericService<Doctor> {

}
