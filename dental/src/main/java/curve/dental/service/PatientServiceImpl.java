package curve.dental.service;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;

import curve.dental.GenericHandler;
import curve.dental.Validator;
import curve.dental.model.Patient;

public class PatientServiceImpl implements PatientService {

	@Autowired
	private GenericHandler<Patient> handler;
	
	@Autowired
	private Validator validator;
	
	public Patient findById(int id) {
		return handler.get(id);
	}

	public List<Patient> selectAll() {
		return handler.list();
	}

	public void create(Patient t) {
		if (validator.validate(t)) {
			handler.create(t);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
	}

	public Patient update(Patient t) {
		if (validator.validate(t)) {
			t = handler.update(t);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
		return t;
	}

	public void delete(Patient t) {
		handler.delete(t);
	}

	public void deleteById(int id) {
		handler.delete(id);
	}
	
}
