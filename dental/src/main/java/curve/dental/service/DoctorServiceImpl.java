package curve.dental.service;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;

import curve.dental.GenericHandler;
import curve.dental.Validator;
import curve.dental.model.Doctor;

public class DoctorServiceImpl implements DoctorService {

	@Autowired
	private GenericHandler<Doctor> handler;
	
	@Autowired
	private Validator validator;
	
	public Doctor findById(int id) {
		return handler.get(id);
	}

	public List<Doctor> selectAll() {
		return handler.list();
	}

	public void create(Doctor t) {
		if (validator.validate(t)) {
			handler.create(t);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
	}

	public Doctor update(Doctor t) {
		if (validator.validate(t)) {
			t = handler.update(t);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
		return t;
	}

	public void delete(Doctor t) {
		handler.delete(t);
	}

	public void deleteById(int id) {
		handler.delete(id);
	}
	
}
