package curve.dental.service;

import curve.dental.model.Block;

public interface BlockService extends GenericService<Block> {

}
