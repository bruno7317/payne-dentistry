package curve.dental.service;

import java.io.Serializable;
import java.util.List;

public interface GenericService<T extends Serializable> {

	T findById(int id);
	
	List<T> selectAll();
	
	void create(T t);
	
	T update(T t);
	
	void delete(T t);
	
	void deleteById(int id);
	
}
