package curve.dental.service;

import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import curve.dental.GenericHandler;
import curve.dental.Validator;
import curve.dental.model.Block;
import curve.dental.model.BlockDay;

@Service
public class BlockServiceImpl implements BlockService {
	
	@Autowired
	private GenericHandler<Block> handler;
	
	@Autowired
	private Validator validator;
	
	public Block findById(int id) {
		return handler.get(id);
	}

	public List<Block> selectAll() {
		return handler.list();
	}

	public void create(Block t) {
		if (validator.validate(t)) {
			Set<BlockDay> selectedDays = t.getSelectedDays();
			t.setSelectedDays(null);
			handler.update(t);
			List<Block> list = handler.list();
			t = list.get(list.size() - 1);
			
			for (BlockDay blockDay : selectedDays) {
				t.addSelectedDay(blockDay);
			}
			handler.update(t);
			
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
	}

	public Block update(Block t) {
		return handler.update(t);
	}

	public void delete(Block t) {
		handler.delete(t);
	}

	public void deleteById(int id) {
		handler.delete(id);
	}
	
}
