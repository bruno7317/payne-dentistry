package curve.dental.service;

import curve.dental.model.Appointment;

public interface AppointmentService extends GenericService<Appointment> {

}
