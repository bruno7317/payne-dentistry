package curve.dental.service;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import curve.dental.GenericHandler;
import curve.dental.Validator;
import curve.dental.model.Appointment;

@Service
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private GenericHandler<Appointment> handler;
	
	@Autowired
	private Validator validator;
	
	public Appointment findById(int id) {
		return handler.get(id);
	}

	public List<Appointment> selectAll() {
		return handler.list();
	}

	public void create(Appointment t) {
		if (validator.validate(t)) {
			handler.create(t);
		} else {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Something went wrong", validator.getMessage()));
		}
	}

	public Appointment update(Appointment t) {
		return handler.update(t);
	}

	public void delete(Appointment t) {
		handler.delete(t);
	}

	public void deleteById(int id) {
		handler.delete(id);
	}
	
}
