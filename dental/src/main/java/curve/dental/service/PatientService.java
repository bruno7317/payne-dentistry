package curve.dental.service;

import curve.dental.model.Patient;

public interface PatientService extends GenericService<Patient> {

}
