package curve.dental;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

@Repository
@Transactional
public abstract class AbstractHandler<T extends Serializable> extends SpringBeanAutowiringSupport {

	private Class<T> c;

	@Autowired
	private SessionFactory factory;

	public final void setClass(Class<T> c) {
		this.c = c;
	}

	public T get(int id) {
		return getCurrentSession().get(c, id);
	}

	public List<T> list() {
		return getCurrentSession().createQuery("from " + c.getName()).list();
	}

	public void create(T entity) {
		getCurrentSession().persist(entity);
	}

	public T update(T entity) {
		return (T) getCurrentSession().merge(entity);
	}

	public void delete(T entity) {
		getCurrentSession().delete(entity);
	}

	public void delete(int id) {
		T entity = get(id);
		delete(entity);
	}

	protected final Session getCurrentSession() {
		return factory.getCurrentSession();
	}

}