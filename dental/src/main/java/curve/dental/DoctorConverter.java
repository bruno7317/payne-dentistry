package curve.dental;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Doctor;
import curve.dental.service.DoctorService;

@FacesConverter(value = "doctorConverter")
@Component
public class DoctorConverter extends SpringBeanAutowiringSupport implements Converter {
	
	@Autowired
	private DoctorService doctorService;

	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Doctor doctor = null;
		if (value != null && !value.isEmpty()) {
			int id = Integer.parseInt(value);
			doctor = doctorService.findById(id);
		}
		return doctor;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String r = "";
		if (value != null) {
			Doctor doctor = (Doctor) value;
			int id = doctor.getId();
			r = String.valueOf(id);
		}
		return r;
	}

}
