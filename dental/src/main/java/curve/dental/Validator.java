package curve.dental;

import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import curve.dental.model.Appointment;
import curve.dental.model.Block;
import curve.dental.model.BlockDay;
import curve.dental.model.Doctor;
import curve.dental.model.Patient;

@Component
public class Validator extends SpringBeanAutowiringSupport {

	private final int ONE_WEEK = 7;
	private String message = "";
	
	@Autowired
	private GenericHandler<Block> blockHandler;

	public String getMessage() {
		return message;
	}

	public boolean validate(Doctor doctor) {
		message = "";
		String name = doctor.getName();
		if (name == null || name.trim().isEmpty()) {
			message += "Invalid doctor's name. ";
		}
		return message.trim().isEmpty();
	}

	public boolean validate(Patient patient) {
		message = "";
		String name = patient.getName();
		String phone = patient.getPhone();
		if (name == null || name.trim().isEmpty()) {
			message += "Invalid patient's name. ";
		}
		if (phone == null || phone.trim().isEmpty()) {
			message += "Invalid patient's phone. ";
		}
		return message.trim().isEmpty();
	}

	public boolean validate(Appointment appointment) {
		message = "";
		Date start = appointment.getDateStart();
		Date end = appointment.getDateEnd();
		Doctor doctor = appointment.getDoctor();
		Patient patient = appointment.getPatient();
		if (start == null || start.before(Calendar.getInstance().getTime()) || (end != null && start.after(end))) {
			message += "Invalid start date for appointment. ";
		}
		if (end == null || end.before(Calendar.getInstance().getTime()) || (start != null && end.before(start))) {
			message += "Invalid end date for appointment. ";
		}
		if (doctor == null) {
			message += "An appointment must have a doctor assigned. ";
		}
		if (patient == null) {
			message += "An appointment must have a patient assigned. ";
		}
		if (message.trim().isEmpty() && !isPeriodAvailable(doctor.getId(), start, end)) {
			message += "There is an appointment/blocking already scheduled for this period. ";
		}
		return message.trim().isEmpty();
	}

	public boolean validate(Block block) {
		message = "";
		Date start = block.getDateStart();
		String description = block.getDescription();
		int duration = block.getDuration();
		Doctor doctor = block.getDoctor();
		if (start == null || start.before(Calendar.getInstance().getTime())) {
			message += "Invalid start date for booking block. ";
		}
		if (description == null || description.trim().isEmpty()) {
			message += "Booking block must have a description. ";
		}
		if (duration <= 0) {
			message += "Invalid duration for booking block. ";
		}
		if (doctor == null) {
			message += "A booking block must have a doctor assigned. ";
		}
		int i = 0;
		int repeat = block.getRepeat();
		if (repeat > 0) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(start);
			a: do {
				for (BlockDay blockDay : block.getSelectedDays()) {
					cal.set(Calendar.DAY_OF_WEEK, blockDay.getWeekday());
					if (message.trim().isEmpty() && !isBlockPeriodAvailable(doctor.getId(), cal.getTime(), duration)) {
						message += "There is an appointment/blocking already scheduled for this period. ";
						break a;
					}
				}
				cal.add(Calendar.DAY_OF_YEAR, ONE_WEEK);
				i++;
			} while (i < repeat);
		}
		return message.trim().isEmpty();
	}

	private boolean isBlockPeriodAvailable(int doctor, Date start, int duration) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.MINUTE, duration);
		return isPeriodAvailable(doctor, start, cal.getTime());
	}

	private boolean isPeriodAvailable(int doctor, Date start, Date end) {
		return blockHandler.isPeriodAvailable(doctor, start, end);
	}
	
}
