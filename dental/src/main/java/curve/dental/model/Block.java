package curve.dental.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Block implements Serializable {

	private static final long serialVersionUID = -5575078843955005157L;

	@Id
	private int id;

	private String description;

	@Column(name = "date_start")
	private Date dateStart;

	private int duration;

	@Transient
	private int repeat;

	@ManyToOne
	@JoinColumn(name = "doctor_id")
	private Doctor doctor;

	@OneToMany(mappedBy = "block", fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private Set<BlockDay> selectedDays;

	@Transient
	private boolean view;

	public Block() {
	}

	public Block(String description, Date dateStart, int duration, Doctor doctor) {
		this.description = description;
		this.dateStart = dateStart;
		this.duration = duration;
		this.doctor = doctor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public int getRepeat() {
		return repeat;
	}

	public void setRepeat(int repeat) {
		this.repeat = repeat;
	}
	
	public void addSelectedDay(BlockDay blockDay) {
		if (selectedDays == null) {
			selectedDays = new HashSet<BlockDay>();
		}
		selectedDays.add(blockDay);
		blockDay.setBlock(this);
	}
	
	public void removeSelectedDay(BlockDay blockDay) {
		blockDay.setBlock(null);
		selectedDays.remove(blockDay);
	}

	public Set<BlockDay> getSelectedDays() {
		return selectedDays;
	}

	public void setSelectedDays(Set<BlockDay> selectedDays) {
		this.selectedDays = selectedDays;
	}

	public boolean isView() {
		return view;
	}

	public void setView(boolean view) {
		this.view = view;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Block other = (Block) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
