package curve.dental.model;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class BlockDay {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private int weekday;

	@ManyToOne
	@JoinColumn(name = "block_id", nullable = false)
	private Block block;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getWeekday() {
		return weekday;
	}

	public void setWeekday(int weekday) {
		this.weekday = weekday;
	}

	public Block getBlock() {
		return block;
	}

	public void setBlock(Block block) {
		this.block = block;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + weekday;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BlockDay other = (BlockDay) obj;
		if (id != other.id)
			return false;
		if (weekday != other.weekday)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.valueOf(weekday);
	}
	
	

}
